﻿; Default script to setup vars and properties to increase performance
; and reduce repeated code in all scripts

#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.

#NoTrayIcon ; Hide AHK system tray icon
SetKeyDelay, 0 ; Disable delay between simulated key presses