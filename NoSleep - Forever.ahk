#SingleInstance Force
#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.
Menu, Tray, Tip, NoSleep: Forever`nKeeping PC awake endlessly

Loop {

	MouseGetPos, CurrentX, CurrentY
	If (CurrentX = LastX and CurrentY = LastY) {
		MouseMove, 1, 1,,R
		Sleep, 100
		MouseMove, -1, -1,,R
	}
	Sleep 60000
	LastX := CurrentX
	LastY := CurrentY
}

return
