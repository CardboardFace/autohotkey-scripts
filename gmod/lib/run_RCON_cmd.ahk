RunAHKStringInSRCDS(consoleCommand)
{
	; If keys are entered too rapidly the case will scramble:
	SetKeyDelay, 0, 1

	; Get all console windows
	WinGet, consoleWindows, List, ahk_class ConsoleWindowClass,
	
	; Loop through all running console apps
    Loop, %consoleWindows%
	{
		; Get the process name
		WinGet, PName, ProcessName, % "ahk_id " consoleWindows%A_Index%
		
		; Check if the console process is SRCDS
		If (PName = "srcds.exe") {
			ControlSend,, %consoleCommand%{ENTER}, % "ahk_id " consoleWindows%A_Index%
		}
	}
	
}