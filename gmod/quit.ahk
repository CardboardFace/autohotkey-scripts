#Include %A_ScriptDir%\..\lib\fredsmacros.ahk
#Include %A_ScriptDir%\lib\run_RCON_cmd.ahk


; Close watchdog server batch that auto-restarts crashed/quitted servers
; Get all console windows
WinGet, consoleWindows, List, ahk_class ConsoleWindowClass,

; Loop through all running console apps
Loop, %consoleWindows%
{
	; Get the process name
	WinGetTitle, PTitle, % "ahk_id " consoleWindows%A_Index%
	
	; Lower the case of the title
	StringLower, PTitleLower, PTitle
	
	Haystack = %PTitleLower%
	Needle = watchdog
	IfInString, Haystack, %Needle%
	{
		WinClose, %PTitle% ; Use the original non-lowercase title to close the console window
		break
	}
	
}

RunAHKStringInSRCDS("quit")