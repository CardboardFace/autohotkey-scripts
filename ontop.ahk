#Include %A_ScriptDir%\lib\fredsmacros.ahk

WinSet, AlwaysOnTop, Toggle, A

; Flash the window so you know which one got changed
hWnd := WinActive( "A" )
Loop 8 {
	DllCall( "FlashWindow", UInt,hWnd, Int,True )
	Sleep 70
}
DllCall( "FlashWindow", UInt,hWnd, Int,False )