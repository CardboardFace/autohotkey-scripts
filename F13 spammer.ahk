; Written by Freddie Chessell
; Allows the F13 key to be bound to spam certain keys

#Persistent  ; Keep the script running until the user exits it.
#SingleInstance ; Only allow one instance of this script
#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.


; ---------------  SETTINGS:  ---------------
ShowNotifications := 0 ; Show notifications when settings are changed
global ToggleSpam := 1 ; Setup default toggle setting
global SpamMode := 1 ; Setup default spam mode
global LogitechKeyboardLighting := 1 ; 1 to enable toggling keyboard lights with the macro while it's in use
SpamBrightness := 50 ; The Logitech brightness while spamming
NormalBrightness := 0 ; The Logitech brightness while not spamming



; ---------------  Program vars:  ---------------
global ToggleStatus := 0




if (LogitechKeyboardLighting) {
	hModule := DllCall("LoadLibrary", "Str", "lib\LogitechLedEnginesWrapper.dll", "Ptr")  ; Load DLL into memory
	
	; Warn the user if LogitechKeyboardLighting is unsupported (missing or broken DLL):
	if not DllCall("LogitechLedEnginesWrapper\LogiLedInit") {
		MsgBox, Could not initialize Logitech DLL, disabling Logitech RGB colouring!
		LogitechKeyboardLighting := 0
	
	} else {
		Sleep, 500 ; Logitech documentation claims it need some time to warm up, can probably be lowered
		SetLogitechDevicesRGBBrightness(NormalBrightness) ; Set default brightness
	}
}


; Ensure the keyboard lights are off after wake from sleep or startup if AHK loads before Logitech
KeepLightsOff() {
	if (ToggleSpam = 0 or (ToggleSpam and ToggleStatus = 0)) {
		SetLogitechDevicesRGBBrightness(NormalBrightness)
	}
	Sleep 2000
}
SetTimer KeepLightsOff




; ---------------  System tray menu:  ---------------

; Cleanup tray menu items
Menu, Tray, NoStandard

; Add option to reload the current script (in case changes were made)
MenuReloadScriptText := "Reload script"
Menu, Tray, Add, %MenuReloadScriptText%, MenuHandler

; Add option to exit the current script
MenuExitScriptText := "Exit script"
Menu, Tray, Add, %MenuExitScriptText%, MenuHandler

; Change the tray icon
STAR_ICON := 44
HD_STAR_ICON := 209
BLUE_STAR_ICON := 319
Menu, Tray, Icon, shell32.dll, %BLUE_STAR_ICON%

; Creates a separator line
Menu, Tray, Add

; Create greyed out title button
MenuTitle := "Settings:"
Menu, Tray, Add, %MenuTitle%, MenuHandler
Menu, Tray, Disable, %MenuTitle%

; Add left click spam option
global MenuToggleSpam := "Toggle spam"
Menu, Tray, Add, %MenuToggleSpam%, MenuHandler  ; Creates a new menu item.


; Creates a separator line
Menu, Tray, Add

; Create greyed out title button
MenuTitle := "Key to spam:"
Menu, Tray, Add, %MenuTitle%, MenuHandler
Menu, Tray, Disable, %MenuTitle%

; Add left click spam option
global MenuLeftClickText := "Left click"
Menu, Tray, Add, %MenuLeftClickText%, MenuHandler  ; Creates a new menu item.

; Add spacebar spam option
global MenuSpaceSpamText := "Space"
Menu, Tray, Add, %MenuSpaceSpamText%, MenuHandler  ; Creates a new menu item.




; Load settings into GUI
SetToggleSpam(ToggleSpam)
SetSpamMode(SpamMode)




return ; Stop handlers below being called on the first script load











; Function to change all Logitech device brightness (from 0-100) if enabled
SetLogitechDevicesRGBBrightness(brightness) {
	if (LogitechKeyboardLighting) {
		DllCall("LogitechLedEnginesWrapper\LogiLedSetLighting", "Int", brightness, "Int", 0, "Int", 0) ; Set base light to all keys
	}
}

SetToggleSpam(enabled) {
	ToggleSpam := enabled
		
	if (enabled) {
		Menu, Tray, Check, %MenuToggleSpam%
	} else {
		Menu, Tray, Uncheck, %MenuToggleSpam%
	}
}

SetSpamMode(mode) {
	SpamMode := mode
		
	if (mode = 0) {
		Menu, Tray, Check, %MenuLeftClickText%
		Menu, Tray, Uncheck, %MenuSpaceSpamText%
		
	} else if (mode = 1) {
		Menu, Tray, Check, %MenuSpaceSpamText%
		Menu, Tray, Uncheck, %MenuLeftClickText%
	}
}


MenuHandler:
	if (A_ThisMenuItem = MenuReloadScriptText) {
		Reload
		return
		
	} else if (A_ThisMenuItem = MenuExitScriptText) {
		ExitApp
	
	} else if (A_ThisMenuItem = MenuToggleSpam) {
		SetToggleSpam(!ToggleSpam)
	
	} else if (A_ThisMenuItem = MenuLeftClickText) {
		SetSpamMode(0)
		
	} else if (A_ThisMenuItem = MenuSpaceSpamText) {
		SetSpamMode(1)
	}
	
	if (ShowNotifications) {
		StringUpper MenuItemTextUppered, A_ThisMenuItem ; Lower the string case
		
		TrayTip F13 spammer, Switched to %MenuItemTextUppered% mode
		
		; ModeChangeTitle := "Mode updated"
		; SplashBoxWidth = 230
		; SplashBoxHeight = 20
		; SplashTextOn, 230, 20, %ModeChangeTitle%, Switched to %MenuItemTextUppered% mode
		; 
		; SysGet, VirtualScreenWidth, 78
		; SysGet, VirtualScreenHeight, 79
		; 
		; WinMove, %ModeChangeTitle%, , VirtualScreenWidth / 2 - SplashBoxWidth / 2, VirtualScreenHeight - SplashBoxHeight - VirtualScreenHeight * 0.3
		; Sleep 2500
		; SplashTextOff
	}

	return

	
	
; Function to simulate input spam
FireSpam() {
	if (SpamMode = 0) {
		click
		
	} else if (SpamMode = 1) {
		Send {space down}
		Sleep 2
		Send {space up}
	}
}





F13::
	if (ToggleSpam) {
		ToggleStatus := !ToggleStatus
		
		if (ToggleStatus = 1) {
			SetLogitechDevicesRGBBrightness(SpamBrightness)
		} else {
			SetLogitechDevicesRGBBrightness(NormalBrightness)
		}

		SetTimer FireSpam, % ToggleStatus ? "50" : "off"
		KeyWait, F13 ; Wait for F13 to be released before listening for further repeats by the OS (preventing rapid spam toggling)
		
	} else {
		SetLogitechDevicesRGBBrightness(SpamBrightness)
		FireSpam()
	}
	return

	
F13 UP::
	if (!ToggleSpam) {
		SetLogitechDevicesRGBBrightness(NormalBrightness)
	}
	return