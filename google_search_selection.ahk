﻿#Include %A_ScriptDir%\lib\fredsmacros.ahk



clipsaved:= ClipboardAll	; This line is here so the original clipboard contents can be restored when the script is finished.
Send, ^c	 ; Copy selection
Sleep 50	; Give time for copy to save

StringLen, TextLength, Clipboard


If (%TextLength% = 0) {
	Clipboard := clipsaved    ; Sets the clipboard back before using MsgBox incase the user misses the message immediatly
	MsgBox, You must select something to search!
} else {
	StringReplace, formattedClipboard, Clipboard, ", "", 1 		; Double up quotes to escape them so AHK "Run" command can use the string
	
	Run, http://www.google.com/search?q=%formattedClipboard%
	Clipboard := clipsaved    ; Sets the clipboard back to whatever it was before
}