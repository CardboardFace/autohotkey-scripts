; Written by Freddie Chessell
; Forces volume wheels that increment volume by 4 steps to use user-defined preference (VolIncrementAmount)



#MaxHotkeysPerInterval 200
#Persistent  ; Keep the script running until the user exits it.
#SingleInstance ; Only allow one instance of this script
#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
#NoTrayIcon ; Hide the tray icon

; Settings
VolIncrementAmount := 2 ; How much an increment of the volume wheel should change the windows volume


; Vars
BlockVolUp := false
BlockVolDown := false

; Function to force Windows volume OSD to popup
ShowVolHUD() {
	Send, {Volume_Up 1}
	Send, {Volume_Down 1}
}

Return


$Volume_Up::
	ShowVolHUD()
	BlockVolUp := !BlockVolUp
	if (BlockVolUp) {
		SoundSet +VolIncrementAmount
	}
	Return

$Volume_Down::
	ShowVolHUD()
	BlockVolDown := !BlockVolDown
	if (BlockVolDown) {
		SoundSet -VolIncrementAmount
	}
	Return