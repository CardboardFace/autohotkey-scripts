; Written by Freddie Chessell
; Script simply toggles my LIFX bulb
#Include %A_ScriptDir%\lib\fredsmacros.ahk

#Include %A_ScriptDir%\lib\LIFX.ahk

MyLight := new LIFXLight("Lamp") ; Given the name of your light you want to control.
try {
	MyLight.Toggle()
} catch {}