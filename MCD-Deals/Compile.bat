@echo off
title compiler...
cls

echo Recompiling initiated

echo Killing apps...
taskkill /F /IM "MCD deals.exe"

echo.
echo Recompiling...
start "" "C:\Program Files\AutoHotkey\Compiler\Ahk2Exe.exe" /in "MCD deals.ahk" /icon "Icon.ico"

echo.
echo.
echo Recompile finished!
echo Press any key to exit...
pause>nul