#Persistent  ; Keep the script running until the user exits it.
#SingleInstance force ; Only allow one instance of this script and don't prompt on replace
#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.
SettingsName := "MCDDeals.ini"

#Include lib\lib.ahk
#Include lib\JSON.ahk

; Settings
global debugMode := false ; Set to true to pause after mirroring tweets without updating LastTweetID (no need to tweet every test, simply un-pause)
global notificationTime := 15 ; Time notifications stay onscreen in seconds

; Global vars (leave empty, `global` shares them with included scripts)
global guiHeight :=
global lastNotification := -(notificationTime*1000)


; Global vars (hard-coded settings)
NotificationPollRate := 55000 ; Check if notifications should show every 55 seconds
ValidDeals := 0 ; Last total number of deals found
ERROR_ICON := 78
LOADING_ICON := 123
DEFAULT_ICON := StrReplace(A_ScriptFullPath, ".ahk", ".exe")



; Cleanup tray menu items
Menu, Tray, Tip, McDonald's Deals
Menu, Tray, Icon, %DEFAULT_ICON%
Menu, Tray, NoStandard

; Add credits button
MenuAboutText := "About"
Menu, Tray, Add, %MenuAboutText%, MenuHandler

; Add change settings button
MenuShowDealsText := "Show deals"
Menu, Tray, Add, %MenuShowDealsText%, MenuHandler
Menu, Tray, Default, %MenuShowDealsText%

; Creates a separator line
Menu, Tray, Add

; Add option to reload the current script (in case changes were made)
MenuReloadScriptText := "Restart"
Menu, Tray, Add, %MenuReloadScriptText%, MenuHandler

; Add option to exit the current script
MenuExitScriptText := "Exit"
Menu, Tray, Add, %MenuExitScriptText%, MenuHandler



SetGenericHeaders(byRef httpSession, tokenToUse) {
	httpSession.SetRequestHeader("Authorization", "Bearer " . tokenToUse)
	httpSession.SetRequestHeader("Content-Type", "application/json")
	httpSession.SetRequestHeader("User-Agent", "Arch/1.0 CFNetwork/1121.2.2 Darwin/19.2.0")
	httpSession.SetRequestHeader("mcd-sourceapp", "MOT")
	httpSession.SetRequestHeader("mcd_apikey", "UKDCUSIOSPILOT9195414KIDLEU4480SD43T500UK")
	httpSession.SetRequestHeader("mcd-marketid", "UK")
	httpSession.SetRequestHeader("mcd-locale", "en-GB")
	httpSession.SetRequestHeader("mcd-apiuid", "644e1dd7-2a7f-18fb-b8ed-ed78c3f92c2bsd")
	httpSession.SetRequestHeader("MarketId", "UK.PROD3")
}

GetNewMCDAuthToken(LastValidToken) {
	tokenFetcher := ComObjCreate("WinHttp.WinHttpRequest.5.1")
	tokenFetcher.Open("POST", "https://europe.api.mcd.com/v2/customer/security/authentication/refresh", true)
	
	SetGenericHeaders(tokenFetcher, LastValidToken)
	tokenFetcher.Send()
	
	; Wait for response to load
	tokenFetcher.WaitForResponse()
	sleep 200
	tokenJSON := tokenFetcher.ResponseText
	
	if (RegexMatch(deal.Name, "You don't have permission to access(.*)on this server") > 0) {
		throw, "Your IP is blocked from communicating with McDonald's! Try disabling any VPNs/proxies."
	}
	
	; Parse JSON string into object
	authObj := JSON.Load(tokenJSON)

	return authObj.details.token
}


; Download file from URL with support for headers
;   authToken is the MCD token
;   URL is the full path to the file
;   newFileFullPath is the full local path where the file should be downloaded to
; Credits: https://www.reddit.com/r/AutoHotkey/comments/6fa5mw/download_a_file/dihaznv/
; Credits: https://autohotkey.com/board/topic/74719-function-remoteresource-for-images-etc/
downloadMCDFile(authToken, url, newFileFullPath) {
	static r := false, obj := ComObjCreate("WinHTTP.WinHttpRequest.5.1")
	
	fo := fileOpen(newFileFullPath, "w")
	if !fo
		return 0	; file error

	if !r || (obj.Option(1) != url) {
		obj.Open("GET", url)
	}
	SetGenericHeaders(obj, authToken)
	
	try {
		obj.Send()
	} catch {
		ShowError("Connection to MCD file timed out:\n" . url)
		return -1 ; Connection timed out
	}
	
	if (obj.ResponseText = "failed") || (obj.Status != 200) || (comObjType(obj.ResponseStream) != 0xd)
		return -1	; server error
	
	iptr := comObjQuery(obj.ResponseStream, "{0000000c-0000-0000-C000-000000000046}")
	loop {
		varsetcapacity(buf, 8192)
		r := dllCall(numGet(numGet(iptr + 0) + 3 * a_ptrsize), ptr, iptr, ptr, &buf, uint, 8192, "ptr*", bytes)
		fo.rawWrite(&buf, bytes)
	} until (bytes = 0)

	objRelease(iptr)
	fo.Close()
	return 1	; download complete
}

GetMCDResponseFromURL(authToken, url) {
	http := ComObjCreate("WinHttp.WinHttpRequest.5.1")
	http.Open("GET", url, false)
	SetGenericHeaders(http, authToken)
	http.Send()
	
	return http.ResponseText
}

; Checks to see if date (in formats YYYY MM DD) is older than current date
HasDealExpired(DealExpiryYear, DealExpiryMonth, DealExpiryDay) {
	FormatTime, CurrYear,, yyyy
	FormatTime, CurrMonth,, MM
	FormatTime, CurrDay,, dd
	
	DealExpired := 0
	if (CurrYear > DealExpiryYear) {
		DealExpired := 1
	} else if (CurrYear == DealExpiryYear) {
		if (CurrMonth > DealExpiryMonth) {
			DealExpired := 1
		} else if (CurrMonth = DealExpiryMonth) {
			if (CurrDay > DealExpiryDay) {
				DealExpired := 1
			}
		}
	}
	
	return DealExpired
}

; Uses the last saved auth token to generate a new valid auth token
GetMCDAuthToken() {
	global
	
	; Retrieve last auth token, using a valid fallback if none is cached
	fallbackAuthToken := "eyJhbGciOiJBMTI4S1ciLCJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwidHlwIjoiSldUIiwia2lkIjoiMjAxNy0wMi0wOCJ9.OxDTtp2ZSGUDxYxfbVsWNqJ8GLTeJ5iy2ndKH_0OeYCieUxn38TtCA.CVaQ-ks2BoKTEyiX_AMnYA.nAnC_H3-sUsTTpashY-zEY8Zvy1fU4kljDs9bc4hKQdUwfEHTcoqLqSxFbCAI7FQCyDWYKEng45rB5l3oW46Nke-vmYKaVp5y_Jx_3G6J0H3TJhreJB1VNNT5Dxj5pOs6Kyh4uwVmj759RqDSHxFdjIzti9DMABadvm53P7yYc-aylwmtOwEbW8TXUzmRCPJzyEhj_9WC2RAUhSk0kyPWzUgNJvNEjjHG7MnD62g0kAhnw2AeAVS0f4Xglvjue0mYttBp_jK4hKjxpe7GSD-ZA4STtSwTLfA-EY0HW6LI765JXTc-JynAoiu_Vhi0RHW6NalVg0dCWDTPFaDs_hOWa83V4zfkYQEnYdMqEAlnWj5O-nOQYaEkNPfDk42G2rnLiEhgKaNJZ58taVvvzPLdsYhT3mX_oEFysYoUgTsudG8u19LZRW3_-AA7RT5xel_NpJS5w9P9MJyeqEjc6OCrNkMSsZw7s_x5sjuTNCXmROCH9nIr1MRjRoCpqPdBpTvqkRkHEMAoZ6lj7axiNKROfT6QEiVT7we-0UXp83C4-e0Dk3g6ZMVDr_Crb2LYzkwT7JYjsIGHYs1NMxzdcTAodWf3hZ8pmU1fW4RIzGNmx-n3FQMg5-182Gps9Ygm95U8vTH6vYbHWop5tIBvi9Z5ClaLX4TRXt_7JWz8_yrMv8.wJtx7jL6WQSGRYAqbi0ttg"
	IniRead, PrevAuthToken, %SettingsName%, Settings, LastValidToken, %fallbackAuthToken%

	; Generate new auth token in case needed
	authToken := GetNewMCDAuthToken(PrevAuthToken)

	; Save last to ini file
	IniWrite, %authToken%, %SettingsName%, Settings, LastValidToken
	
	return authToken
}


; Checks for new deals, updating GUI in background (in case shown) and updating ValidDeals count
LoadNewMCDDeals() {
	global
	
	; Clear old GUI
	Gui, Destroy
	
	; Get new deals JSON
	mcdDomain := "https://europe.api.mcd.com/"
	mcdDealsURL := "v3/customer/offer"
	mcdParams := "?application=MOT&languageName=en-GB&marketId=UK.PROD3&platform=iphone"
	try {
		authToken := GetMCDAuthToken()
	} catch e {
		MsgBox, Error generating auth token: %e%
		Reload
	}
	
	replyMsg := GetMCDResponseFromURL(authToken, mcdDomain . mcdDealsURL . mcdParams)
	MyDeals := JSON.Load(replyMsg) ; Convert JSON to object
	
	; Restore default icon for window
	Menu, Tray, Icon, %DEFAULT_ICON%

	rowPadding := 30
	topMargin := 5
	dealHeight := topMargin + rowPadding + 140
	
	elemOffset := topMargin
	ValidDeals := 0
	for index, deal in MyDeals.Data ; Enumeration is the recommended approach in most cases.
	{
		DealParts := StrSplit(deal.Name, "`n") ; Deal name and description originally stored in one string separated by newline
		DealName := DealParts[1]
		DealDesc := DealParts[2]
		DealExpiresStr := deal.Conditions.DateConditions[1].to
		DealImageFullURL := deal.ImageBaseName ; Find full image path
		RegExMatch(DealImageFullURL, "([^\/]+$)", DealImageFileName) ; Use regex to find the file name
		DealImageFullLocalPath := "cache\" . DealImageFileName ; Build a local path
		
		; Attempt to download the image
		;   If DL fails, a tiny text file (<2kb) will be stored using the same
		;   name!
		downloadMCDFile(authToken, mcdDomain . DealImageFullURL, DealImageFullLocalPath)
		
		; Check if the image is too small
		; If it is, use fallback image
		FileGetSize, DealImageSize, DealImageFullLocalPath, K
		if (DealImageSize < 2) {
			DealImageFullLocalPath := "happy-meal.png"
		}

		; Don't show expired deals
		DealExpiryYear := SubStr(DealExpiresStr, 1, 4)
		DealExpiryMonth := SubStr(DealExpiresStr, 6, 2)
		DealExpiryDay := SubStr(DealExpiresStr, 9, 2)
		if (HasDealExpired(DealExpiryYear, DealExpiryMonth, DealExpiryDay)) {
			continue
		}
		
		; Don't show McCafe rewards scheme - it's always active and has a weird
		; format.
		if (RegexMatch(deal.Name, "Caf(.*) Rewards\nBuy 5 McCaf(.*)") > 0) {
			continue
		}
		
		; Increment valid deals now all bad ones are filtered out
		ValidDeals++
		
		; Calculate GUI control positions and sizings
		picY := 9 + elemOffset
		dealNameY := 10 + elemOffset
		DealExpireY := 90 + elemOffset
		dealDescY := 35 + elemOffset

		; Add multi-line if name is too long
		maxLineWidth := 46 ; How many chars in a line
		dealNameLineHeight := 22 ; How tall the deal's name lines are
		if (strLen(DealName) > maxLineWidth) {
			dealNameLines := Floor(strLen(DealName) / maxLineWidth)
			dealDescY += dealNameLineHeight * dealNameLines
		}
		
		Gui Add, Picture, x420 y%picY% w115 h115, %DealImageFullLocalPath%
		
		Gui Font, s12 Norm c0x7C13D2, Roboto Medium
		Gui Add, Text, x10 y%dealNameY% +Wrap w400, %DealName%
		
		Gui Font
		Gui Font, s12 Norm, Roboto Light
		Gui Add, Text, x11 y%dealDescY% w400 h64, %DealDesc%
		
		Gui Font
		Gui Font,, Roboto
		Gui Add, Text, x10 y%DealExpireY% +Wrap w196 h25, Offer expires %DealExpiryDay%/%DealExpiryMonth%/%DealExpiryYear%
		Gui Font
		
		elemOffset := ValidDeals * dealHeight
	}
	guiHeight := Max(dealHeight, elemOffset - rowPadding)
	
	if (ValidDeals == 0) {
		Gui Font
		Gui Font, s18 Bold, Roboto
		Gui Add, Text, x130 y50 +Wrap w400 h25, No deals at the moment...
		Gui Font
	}
	
	return true
}

ShowError(msg) {
	global
	Menu, Tray, Icon, shell32.dll, %ERROR_ICON%
	Menu, Tray, Tip, %msg%
}

CheckForNewDeals() {
	global
	
	; Update tray text & icon
	Menu, Tray, Tip, Checking for new deals...
	Menu, Tray, Icon, shell32.dll, %LOADING_ICON%
	

	; Check for new deals
	ProcessSuccessful := LoadNewMCDDeals()
	
	if (!ProcessSuccessful) {
		ShowError("Error occurred when polling MCD...")
		try {
			authToken := GetMCDAuthToken()
		} catch e {
			ShowError("Error generating auth key when polling MCD: %e%")
		}
	} else {
		; Restore default tray
		Menu, Tray, Icon, %DEFAULT_ICON%
		Menu, Tray, Tip, McDonald's Deals
	}
}

StartDealsChecker() {
	global
	
	; Clear cached deal images
	FileRemoveDir, "cache"
	FileCreateDir, "cache"
	
	; Endlessly run checks for meal times
	Loop {
		FormatTime, CurrMin,, m
		FormatTime, CurrHour,, H ; 24 hour time
		
		ApproachingBreakfast := (CurrMin = 0 and CurrHour = 8)
		ApproachingLunch := (CurrMin = 30 and CurrHour = 10)
		ApproachingDinner := (CurrMin = 30 and CurrHour = 4)
		
		; Only poll McDonald's if it's time to show notifications
		if (ApproachingBreakfast or ApproachingLunch or ApproachingDinner) {
			CheckForNewDeals()
			
			if (ValidDeals > 0) {
				lastNotification := A_TickCount
				TrayTip, McDonald's deals, McDonald's has %ValidDeals% deals on today!, %notificationTime%
			}
		}
		
		Sleep, NotificationPollRate
	}
}
StartDealsChecker()


ShowDealsUI() {
	global
	
	Gui, Show, w540 h%guiHeight% Center, MCD Deals
	Gui, +LastFound +AlwaysOnTop ; Make window always on top
}

return ; Stop handlers running on script start


; Add escape key hotkey to dismiss UI
GuiEscape: 
	Gui, Destroy 
	return




MenuHandler:
	if (A_ThisMenuItem = MenuReloadScriptText) {
		Reload
	} else if (A_ThisMenuItem = MenuShowDealsText) {
		CheckForNewDeals()
		ShowDealsUI()
	} else if (A_ThisMenuItem = MenuExitScriptText) {
		ExitApp
	} else if (A_ThisMenuItem = MenuAboutText) {
		MsgBox,0, MCD Deals Credits, Created by Freddie Chessell`, 2020
	}

	return


; Show GUI if user clicks tray tip
~LButton::
	; Check if all script notifications have faded out
	if ((notificationTime * 1000) + lastNotification < A_TickCount) {
		return
	}
	
	MouseGetPos, , , WinVar
	If WinVar = 0x6007a
		ShowDealsUI()