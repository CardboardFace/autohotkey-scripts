; Written by Freddie Chessell
; Gives control over LIFX bulb via Windows taskbar

#Persistent  ; Keep the script running until the user exits it.
#SingleInstance ; Only allow one instance of this script
#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.

#Include %A_ScriptDir%\lib\LIFX.ahk
#Include %A_ScriptDir%\lib\JSON.ahk

; ---------------  Settings:  ---------------
global MyLight := new LIFXLight("Lamp") ; Given the name of your light you want to control.
global brightness := 100 ; Local var used for UI checking/unchecking
global UpdateStatusInterval := 30000 ; How often, in milliseconds, to update from API (in case user light from other devices)



; Gets current light status JSON string and prints to messagebox for debugging
DisplayLightStatus() {
	StatusString := MyLight.LightStatus()
	MsgBox, %StatusString%
}

; Gets light status from API server and loads current settings into UI
GetLightStatus() {
	try {
		LightJSON := MyLight.LightStatus()
		
		; If LIFX lib can't load the request, show error icon:
		if (!LightJSON) {
			ERROR_ICON := 110
			Menu, Tray, Icon, shell32.dll, %ERROR_ICON%
			return
		}
	} catch e {
		Return
	} ; Catch errors when no API access, for instance while PC is locked
	
	LightStatus := JSON.Load(Trim(LightJSON, OmitChars:=" []")) ; Remove spaces and square brackets surrounding JSON then load into object
	
	if (LightStatus.seconds_since_seen > 30) {
		ERROR_ICON := 110
		Menu, Tray, Icon, shell32.dll, %ERROR_ICON%
	} else if (LightStatus.power == "on") {
		Menu, Tray, Icon, %A_ScriptDir%\icons\LIFX\on.ico
	} else {
		Menu, Tray, Icon, %A_ScriptDir%\icons\LIFX\off.ico
	}
	
	Menu, Tray, UnCheck, % "&" brightness "%" ; Undo local var check (hardcoded at app launch)
	brightness := Floor(Round(LightStatus.brightness, 1) * 100) ; Round to nearest .1, *100 to convert range 0.0-1.0 to percentage (0.0-100.0), floor to remove decimal
	
	; If brightness rounded to 0, show as 10%
	if (brightness < 10) {
		brightness:= 10
	}
	
	Menu, Tray, Check,% "&" brightness "%" ; Check menu item corrisponding to brightness pulled from status
}

; ---------------  System tray menu:  ---------------

; Cleanup tray menu items
Menu, Tray, Tip, LIFX Light
Menu, Tray, NoStandard

; Add option to print status string
Menu, Tray, Add, Print light status, DisplayLightStatus

; Add option to reload the current script (in case changes were made)
MenuReloadScriptText := "Reload script"
Menu, Tray, Add, %MenuReloadScriptText%, MenuHandler

; Add option to exit the current script
MenuExitScriptText := "Exit script"
Menu, Tray, Add, %MenuExitScriptText%, MenuHandler

; Creates a separator line
Menu, Tray, Add

; Create greyed out title button
MenuTitle := "Power:"
Menu, Tray, Add, %MenuTitle%, MenuHandler
Menu, Tray, Disable, %MenuTitle%

; Toggle light
global MenuLightToggle := "Toggle"
Menu, Tray, Add, %MenuLightToggle%, MenuHandler  ; Creates a new menu item.
Menu, Tray, Default, %MenuLightToggle% ; Set toggle to launch when the default tray action is fired
Menu, Tray, Click, 1 ; Set only one left click needed to fire default action (default is 2)

; Turn off light
global MenuLightOff := "Off"
Menu, Tray, Add, %MenuLightOff%, MenuHandler  ; Creates a new menu item.
; Turn on light
global MenuLightOn := "On"
Menu, Tray, Add, %MenuLightOn%, MenuHandler  ; Creates a new menu item.


; Creates a separator line
Menu, Tray, Add

; Create greyed out title button
MenuTitle := "Brightness:"
Menu, Tray, Add, %MenuTitle%, MenuHandler
Menu, Tray, Disable, %MenuTitle%

Loop,10
  Menu, Tray, Add, % "&" 110-(A_Index*10) "%", BrightnessChangedHandler

GetLightStatus() ; Update status from API

getLightStatusFn := Func("GetLightStatus")
SetTimer, %getLightStatusFn%, %UpdateStatusInterval% ; Periodically check for updates

return ; Stop handlers below being called on the first script load



; Function to handle setting light brightness and changing currently checked menu item
BrightnessChangedHandler:
	Menu, Tray, UnCheck, % "&" brightness "%"
	If A_ThisMenuItem<>
		brightness:=A_ThisMenuItem
		StringTrimLeft, brightness, brightness, 1
		StringTrimRight, brightness, brightness, 1
		
		Menu, Tray, Check, % "&" brightness "%"
		MyLight.SetBrightness(brightness / 100)
	Return



MenuHandler:
	if (A_ThisMenuItem = MenuReloadScriptText) {
		Reload
		Return
		
	} else if (A_ThisMenuItem = MenuExitScriptText) {
		ExitApp
		
	} else if (A_ThisMenuItem = MenuLightToggle) {
		MyLight.Toggle()
	
	} else if (A_ThisMenuItem = MenuLightOn) {
		MyLight.SetPower("on")
		
	} else if (A_ThisMenuItem = MenuLightOff) {
		MyLight.SetPower("off")
	}
	
	GetLightStatus() ; Update UI from server
	Return
