#SingleInstance Force
#NoEnv ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir% ; Ensures a consistent starting directory.
#NoTrayIcon

idleTime := 0
moveFrequency := 60 * 1000
screenOffTime := 60 * 5 * 1000

Loop {

	MouseGetPos, currentX, currentY
	dayTime := (A_Hour > 7 and A_Hour < 17) or (A_Hour == 17 and A_Min <= 30)
	if (dayTime) {
		if (currentX = lastX and currentY = lastY) {
			; Keep PC awake (trips A_TimeIdle)
			MouseMove, 1, 1,,R
			Sleep, 100
			MouseMove, -1, -1,,R

			; Check if screen should sleep
			idleTime += moveFrequency
			if (idleTime > screenOffTime) {

				Process, Exist, scrnsave.scr
				If ErrorLevel = 0
				{
					; SendMessage 0x112, 0xF170, 2, , Program Manager ; Monitor off
					Run, C:\Windows\System32\scrnsave.scr /s ; Blank black screen
				}

			}
		} else {
			idleTime := 0
		}
	}
	Sleep, %moveFrequency%
	lastX := currentX
	lastY := currentY
}

return
