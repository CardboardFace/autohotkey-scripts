# AutoHotkey scripts

A collection of my auto hotkey scripts


### Mac keyboard support
Imitates a windows layout for Mac keyboards on Windows 10 by swapping cmd and alt and adding functionality back to the function keys

### F13 spammer
Rebinds F13 to spam a selected key (chosen from the system tray menu). F13 is not often found on keyboards, and can be assigned on gaming keyboards/mice to redundant keys for this script
For instance, automated space spam can be used in BF4 to win vehicle spawns from the deploy screen

### Auto microphone muter
Checks if you're AFK to mute your microphone, preventing users on TeamSpeak/Discord etc from eavesdropping

### ScrollVolume
Allows you to change the Windows system volume by clicking down and scroll the mouse wheel.

### LIFX light
Control my bedroom light from a PC

### PreciseVolumeWheel
Forces volume wheels that increment volume by 4 (or whatever) steps to use user-defined preference (VolIncrementAmount, default is 2)


### StreamDeck macros
A collection of scripts and icons used for Elgato's stream deck to automate common processes and assist server hosting with GMod (see attached pictures)