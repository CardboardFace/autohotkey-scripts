; Written by Freddie Chessell
; Checks if you're AFK to mute your microphone, preventing users on TeamSpeak/Discord etc from eavesdropping

#Persistent  ; Keep the script running until the user exits it.
#SingleInstance ; Only allow one instance of this script
#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.



; ---------------  SETTINGS:  ---------------

MinutesUntilMute := 7 ; How long until microphone mutes
SoundMuted := "lib\mic_muted.wav" ; Sound to play when muting mic
SoundUnMuted := "lib\mic_activated.wav" ; Sound to play when un-muting mic
DefaultMicVolume := 100 ; Microphone volume percent to restore to when no longer AFK

; Use SoundCardAnalysis.ahk to discover these for your microphone, located inside "dev-tools" folder
global MicrophoneComponentType := "MASTER"
global MicrophoneMixerID := 8

; ---------------  END OF SETTINGS:  ---------------





; Tray:
TRANSMITTING_ICON := 173
AFK_ICON := 168
Menu, Tray, NoStandard ; Cleanup tray menu items
Menu, Tray, Icon, shell32.dll, %TRANSMITTING_ICON% ; Set icon
Menu, Tray, Add, Reload script, ReloadHandler ; Add option to reload the current script (in case changes were made)
Menu, Tray, Add, Exit script, ExitHandler ; Add option to exit the current script


IdleTimeToMute := MinutesUntilMute * 1000 * 60 ; How long until TeamSpeak mutes (5 mins)
Settimer, IdleChecker, 200 ; How frequently to check if user is **NO LONGER** AFK
Return





SetMicVolume(newVol) {
	SoundSet, newVol, %MicrophoneComponentType%, volume, %MicrophoneMixerID% ; Set mic volume
}
ReloadHandler() {
	Reload
}
ExitHandler() { 
	ExitApp
}
Clamp(ByRef Val, Min, Max) {
  If (Val < Min)
    Val := Min
  If (Val > Max)
    Val := Max
}

; Alters system volume while playing loud notification sound (minimum of 10% vol)
PlaySoundCusVol(filePath, volOffset) {
	SoundGet, master_volume
	newVol := master_volume + volOffset
	
	Clamp(newVol, 10, 100)
	
	SoundSet, %newVol% 		; Change volume
	SoundPlay, %filePath%, 1	 	; Play sound
	SoundSet, %master_volume%	; Restore volume
}

IdleChecker:
	if (A_TimeIdlePhysical > IdleTimeToMute) {
		if (!muted_mic) {	; Mic not muted yet
			muted_mic=1  ; Prevent the mic muting on a loop
			SetMicVolume(0) 	; Mute mic
			Menu, Tray, Icon, shell32.dll, %AFK_ICON% ; Set tray icon to AFK
			PlaySoundCusVol(SoundMuted, -50)
		}
		
	} else if (muted_mic) { ; Recent activity and mic muted
		muted_mic=0
		SetMicVolume(DefaultMicVolume) 	; Unmute mic
		Menu, Tray, Icon, shell32.dll, %TRANSMITTING_ICON% ; Restore tray icon
		PlaySoundCusVol(SoundUnMuted, -50)
		
	} else { ; Recent activity and mic not muted
		Sleep 5000 ; Delay next timer iteration to save CPU
	}

	Return
