﻿#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.
#SingleInstance Force
Menu, Tray, Icon, shell32.dll, 45
Menu, Tray, Tip, Cisco auto-login

if 0 < 1 ; The left side of a non-expression if-statement is always the name of a variable.
{
  MsgBox This script requires a password parameter but it only received %0%.
  ExitApp
}
password=%1%

StartAnyConnect(pathToAnyConnect) {
  Menu, Tray, Tip, Starting VPN...
  Run,%pathToAnyConnect%,,,pid
  WinWait,Cisco Secure Client,, 5
  if ErrorLevel
  {
    MsgBox, Failed to find AnyConnect Window
    Exit
  }
  Sleep,250
}

DismissTimeoutErrorsFromSleep() {
  Menu, Tray, Tip, Dismissing session timeout message...
  WinWait,Cisco Secure Client,Your VPN connection has exceeded the session time limit, 1
  if !ErrorLevel
  {
    ControlClick, OK, Cisco Secure Client,Your VPN connection has exceeded the session time limit
    Sleep, 3000
  }
}

QuitIfAlreadyConnected() {
  ControlGetText, ButtonText, Button1
  if ButtonText=Disconnect
  {
    Menu, Tray, Tip, DONE: Already connected!
    WinClose
    Sleep, 10000
    Exit
  }
}

DismissAlreadyConnectingErrors() {
  Menu, Tray, Tip, Dismissing connect in-progress errors...
  WinWait,Cisco Secure Client,Connect already in progress, 2
  if !ErrorLevel
  {
    ControlClick, OK, Cisco Secure Client,Connect already in progress
    Sleep,250
  }
}

AttemptConnectClick() {
  Menu, Tray, Tip, Clicking connect...
  WinWait,Cisco Secure Client,, 5
  ControlGetText, ButtonText, Button1
  if ButtonText=Connect
  {
    ControlClick, Button1
    DismissAlreadyConnectingErrors()
  }
}

FillPassword(password) {
  Menu, Tray, Tip, Filling password...
  WinWait,Cisco Secure Client |
  ControlSetText,Edit2,%password%
  Sleep, 200
}

SubmitLoginForm() {
  Menu, Tray, Tip, Submitting login...
  ControlClick, OK, Cisco Secure Client |
}

StartAnyConnect("C:\Program Files (x86)\Cisco\Cisco Secure Client\UI\csc_ui.exe")
DismissTimeoutErrorsFromSleep()
QuitIfAlreadyConnected()
AttemptConnectClick()
FillPassword(password)
SubmitLoginForm()
Menu, Tray, Tip, DONE: Connected
Menu, Tray, Icon, shell32.dll, 177
Sleep, 45000