#HotkeyInterval 1000
#MaxHotkeysPerInterval 200

; Written by Freddie Chessell
Menu, Tray, Icon, DDORes.dll, 29


; Swap left alt and left cmd (windows key)
LAlt::LWin
LWin::LAlt

; Swap right cmd with open context menu
RWin::AppsKey

; Bind F13 to insert and F14 to scrolllock
F13::Insert
F14::ScrollLock

; F11 for volume down
F11::Send {Volume_Down 2}

; F12 for volume up
F12::Send {Volume_Up 2}