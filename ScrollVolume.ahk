; Written by Freddie Chessell
; Click in scroll wheel and scroll to control volume

#Persistent  ; Keep the script running until the user exits it.
#SingleInstance ; Only allow one instance of this script
#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.





; ---------------  System tray menu:  ---------------

; Cleanup tray menu items
Menu, Tray, NoStandard

; Add option to reload the current script (in case changes were made)
MenuReloadScriptText := "Reload script"
Menu, Tray, Add, %MenuReloadScriptText%, MenuHandler

; Add option to exit the current script
MenuExitScriptText := "Exit"
Menu, Tray, Add, %MenuExitScriptText%, MenuHandler

; Change the tray icon
MOUSE_ICON := 109
SPEAKER_ICON_ICON := 89
SPEAKER_ICON := 92
Menu, Tray, Icon, ddores.dll, %SPEAKER_ICON%


NeedsUndoneCapsToggle := 0


return ; Stop handlers below being called on the first script load








MenuHandler:
	if (A_ThisMenuItem = MenuReloadScriptText) {
		Reload
		return
	} else if (A_ThisMenuItem = MenuExitScriptText) {
		ExitApp
	}

	return

	
	

MButton::
	MButtonDown := 1
	Send {MButton}
	
	return
	
	
MButton UP::
	MButtonDown := 0
	
	return

	
$WheelUp::
	if (MButtonDown) {
		Send {Volume_Up 2}
	} else {
		Send {WheelUp}
	}
	
	return

	
$WheelDown::
	if (MButtonDown) {
		Send {Volume_Down 2}
	} else {
		Send {WheelDown}
	}
	
	return